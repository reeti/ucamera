/**
\author rov
\date 10/06/2015
\version 2.1
 

**/

#include <time.h>
#include <unistd.h>
#include "UCamera.h"

//#include <Magick++.h>

//Base constructor for UModule
UCamera::UCamera(const string &n):
  urbi::UObject(n),
  m_coreApp(NULL)
{
    if(QCoreApplication::instance() == NULL)
    {
        int argc = 1;
        char **argv = new char*[1];
        argv[1] = new char[100];
        sprintf(argv[1], "/bin/urbilib");
        m_coreApp = new QCoreApplication(argc, argv);
    }

    m_thread = new QThread();

    m_cameraThread = new UCameraThread();
    m_cameraThread->moveToThread(m_thread);
    m_thread->start();
    usleep(10000);
    QMetaObject::invokeMethod(m_cameraThread, "sl_Init", Qt::QueuedConnection);

	UBindFunction(UCamera, init);
}

UCamera::~UCamera()
{
    m_thread->quit();
    m_thread->wait();
    delete m_cameraThread;
    delete m_thread;

    if(m_coreApp != NULL)
    {
        delete m_coreApp;
    }
}

int UCamera::init(string _IP, int _port)
{
	file.open("/reetiPrograms/logs/startUpLog.log", ios::app);
	fileError.open("/reetiPrograms/logs/errors.log", ios::app);
	fileServer.open("/reetiPrograms/logs/server.log", ios::app);
	srand(time(NULL));
	//init client
    client=new urbi::UClient(_IP.c_str(), _port);

    file<<"UCamera initialized : "<<__DATE__<<": "<<__TIME__<<" version 2.1.0"<<endl;
	//Urbi usable functions

    UBindFunction(UCamera, takePicture);
    UBindFunction(UCamera, recordVideo);
    UBindFunction(UCamera, stopRecord);

    file<<"UCamera initialized: "<<__DATE__<<": "<<__TIME__<<" version 2.1"<<endl;
	file.close();
    return 0;
}

int UCamera::takePicture(int _camera, string _filename)
{
    QString filename = QString::fromStdString(_filename);
    QMetaObject::invokeMethod(m_cameraThread, "sl_takePicture", Qt::QueuedConnection, Q_ARG(int, _camera), Q_ARG(QString, filename));
    return 0;
}

int UCamera::recordVideo(int _camera, string _filename, string _bitrate, string _x264Preset)
{
    QString filename = QString::fromStdString(_filename);
    QString bitrate = QString::fromStdString(_bitrate);
    QString x264Preset = QString::fromStdString(_x264Preset);
    QMetaObject::invokeMethod(m_cameraThread, "sl_recordVideo", Qt::QueuedConnection, Q_ARG(int, _camera), Q_ARG(QString, filename), Q_ARG(QString, bitrate), Q_ARG(QString, x264Preset));
    return 0;
}

int UCamera::stopRecord(int _camera)
{
    QMetaObject::invokeMethod(m_cameraThread, "sl_stopRecord", Qt::QueuedConnection, Q_ARG(int, _camera));
    return 0;
}



UStart(UCamera);
