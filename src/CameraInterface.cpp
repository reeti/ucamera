#include "CameraInterface.h"
#include <QDebug>
#include <QSettings>

CameraInterface::CameraInterface(QString _cameraDevicePath, QString _audioSourceName):
    m_cameraDevicePath(_cameraDevicePath),
    m_pulseAudioSourceName(_audioSourceName),
    m_videosDir("/home/reeti/Videos"),
    m_picturesDir("/home/reeti/Pictures")
{
    m_recordProcess = new QProcess(this);

    QSettings *cameraSettings = new QSettings("/reetiPrograms/configuration/UCamera.ini", QSettings::IniFormat, this);
    cameraSettings->beginGroup(_cameraDevicePath);
    if(cameraSettings->contains("audioLag"))
    {
        m_audioLag = cameraSettings->value("audioLag").toString();
    }
    else
    {
        m_audioLag = "0.5";
        cameraSettings->setValue("audioLag", m_audioLag);
    }
    cameraSettings->endGroup();
    cameraSettings->sync();
    delete cameraSettings;
}

void CameraInterface::TakePicture(QString _filename)
{
    QProcess p;
    p.setWorkingDirectory(m_picturesDir);
    p.setProgram("avconv");
    p.setArguments(QStringList() << "-y" << "-f" << "video4linux2" << "-i" << m_cameraDevicePath << "-frames" << "1" << _filename);

    p.start();
    if(!p.waitForFinished(5000))
    {
        qDebug() << "avconv failure in takePicture()";
        p.terminate();
    }

    SetPermissionsForAll(m_picturesDir, _filename);

    //qDebug() << "Program : " << p.program() << " " << p.arguments() << " : " << p.readAll();
}

void CameraInterface::RecordVideo(QString _filename, QString _bitrate, QString _x264Preset)
{
    if(m_recordProcess->state() != QProcess::NotRunning)
    {
        StopRecord();
    }

    if(_bitrate.isEmpty())
    {
        _bitrate = "2000k";
    }

    if(_x264Preset.isEmpty())
    {
        _x264Preset = "ultrafast";
    }

    QDir workingDir(m_videosDir);
    m_currentVideoFilename = workingDir.absoluteFilePath(_filename);

    m_recordProcess->setWorkingDirectory(workingDir.path());
    m_recordProcess->setProgram("avconv");
    m_recordProcess->setArguments(QStringList() << "-y" << "-f" << "pulse" << "-i" << m_pulseAudioSourceName << "-f" << "video4linux2" << "-itsoffset" << m_audioLag << "-i" << m_cameraDevicePath << "-c:a" << "mp3" << "-b:a" << "128k" << "-c:v" << "libx264" << "-b:v" << _bitrate << "-preset" << _x264Preset << _filename);

    m_recordProcess->start();

    //qDebug() << "Program : " << m_recordProcess->program() << " " << m_recordProcess->arguments();
}

void CameraInterface::StopRecord()
{
    if(m_recordProcess->state() != QProcess::NotRunning)
    {
        m_recordProcess->terminate();
        if(!m_recordProcess->waitForFinished(3000))
        {
            qDebug() << "UCamera : failed to gracefully stop avconv's video recording, killing";
            m_recordProcess->kill();
            m_recordProcess->waitForFinished(500);
        }
        SetPermissionsForAll(m_videosDir, m_currentVideoFilename);
    }
    else
    {
        qDebug() << "UCamera : no recording active";
    }
}

void CameraInterface::SetPermissionsForAll(QString _workingDir, QString _filename)
{
    QDir dir(_workingDir);
    QFile file(dir.absoluteFilePath(_filename));
    SetPermissionsForAll(file);
}


void CameraInterface::SetPermissionsForAll(QFile &_file)
{
    if(_file.exists())
    {
        _file.setPermissions((QFileDevice::Permissions)0x777);
    }
}
