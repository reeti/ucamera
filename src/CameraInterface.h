#ifndef CAMERAINTERFACE_H
#define CAMERAINTERFACE_H

#include <QFile>
#include <QDir>
#include <QProcess>
#include <QObject>

class CameraInterface : public QObject
{
    Q_OBJECT
public:
    CameraInterface(QString _cameraDevicePath, QString _audioSourceName);

    void TakePicture(QString _filename);

    void RecordVideo(QString _filename, QString _bitrate, QString _x264Preset);
    void StopRecord();

private:
    void SetPermissionsForAll(QString _workingDir, QString _filename);
    void SetPermissionsForAll(QFile &_file);

    QString m_cameraDevicePath;
    QString m_pulseAudioSourceName;
    QProcess *m_recordProcess;
    QString m_currentVideoFilename;

    const QString m_videosDir;
    const QString m_picturesDir;
    QString m_audioLag;
};

#endif
