/** @defgroup UCamera UCamera
 *  Module handling the camera of the reeti.
 * Allows you to take pictures, capture videos , ...
 *  
 */

#ifndef UCAMERA_H
#define UCAMERA_H

//standard library
#include <time.h>
#include <fstream>
#include <stdio.h>
#include <iostream>
#include <string.h>
//Urbi libraries
#include "urbi/uclient.hh"
#include <urbi/uobject.hh>

//Open Cv libraries
#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cxcore.h>

#define QT_NO_EMIT

#include <QCoreApplication>
#include <QThread>
#include <QObject>
#include <QMetaObject>

#include "UCameraThread.h"

using namespace std;
using namespace urbi;

/**
\ingroup UCamera
\class UCamera
\brief class to handle the cameras located in the eyes of Reeti

\date  Dec 04, 2014
\author ROV
\version 2.0.0

@section Use
This is how the UCamera object is instantiated at Reeti system launch :

- var Global.camera = uobjects.UCamera.new(IP, 54001);
 */

class UCamera: public urbi::UObject{
public:
    ///@cond INTERN
    /**
    \brief Constructor: called by urbi to initialize the module
    \param n Name of the UModule
    */

	UCamera(const string &n);

    virtual ~UCamera();
	//INitialisator of Urbi
    /**
    \brief The constructor used by urbi. Initialize the instance of UCamera class.
    - Bind the functions to the server.
    - Create the client to make the module communicate with urbi server.
    @param _IP the IP adress of the Urbi server. Needed to create a channel for communication
    @param _port the port of the Urbi server. Needed to create a channel for communication
    @return 0 for success
    @return 1 on failure
    */
    int init(string _IP, int _port);

    ///@endcond

    /**
    \brief Take a picture and save it in the folder /home/reeti/reetiPrograms/Pictures under the namefile "name".
    \param _camera camera of the video to be saved (0=left, 1=right)
    \param _filename filename of the picture to be saved. Must contain an existing picture extension (.jpg, .bmp, ...)
    \return 0 on success
    \return 1 on failure
    */
    int takePicture(int _camera, string _filename);

    /**
    \brief Start the recording of a video and save it under the name: name in /home/reeti/reetiPrograms/Video.
    The video will be encoded using libx264 and the audio in mp3.
    The function will return immediately.
    \param _camera camera of the video to be saved (0=left, 1=right)
    \param _filename filename of the video to be saved. Must contain a valid extension (.mkv, .avi, ...)
    \param _bitrate for example "500k" ; leave empty for default
    \param _x264Preset for example "veryfast" (see libav documentation); leave empty for default (ultrafast)
    \return 0 on success
    \return 1 on failure
    */
    int recordVideo(int _camera, string _filename, string _bitrate, string _x264Preset);

    /**
    \brief Stop the video recording.
    Does nothing if no recording is active on the specified camera.
    \param _camera camere id to stop (0=left, 1=right)
    \return 0 on success
    \return 1 on failure
    */
    int stopRecord(int _camera);

///@cond INTERN

    /**
    \brief Urbi client for communicating with the server, must be initialized
    */
	urbi::UClient* client;

    /**
    \brief The file where the logs for startup are stored
    */
    ofstream file;
    /**
    \brief The file where the logs for server urbi communications are stored
    */
    ofstream fileServer;
    /**
    \brief The file where the logs for errors are stored
    */
    ofstream fileError;

protected:
    QCoreApplication *m_coreApp;

    QThread *m_thread;
    UCameraThread *m_cameraThread;
///@endcond

};


#endif
