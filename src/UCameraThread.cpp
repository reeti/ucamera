#include "UCameraThread.h"
#include <QDebug>
#include <QDir>

UCameraThread::UCameraThread(QObject *_parent):
    QObject(_parent)
{

}

UCameraThread::~UCameraThread()
{
    std::map<int, CameraInterface*>::iterator it = m_cameras.begin();
    while(it != m_cameras.end())
    {
        delete it->second;
    }
    m_cameras.clear();
}

void UCameraThread::sl_Init()
{
    ReadPulseAudioSource();

    m_cameras.insert(std::make_pair(0, new CameraInterface(QString(CAMERA_PATH_LEFT), m_pulseAudioSourceName)));
    m_cameras.insert(std::make_pair(1, new CameraInterface(QString(CAMERA_PATH_RIGHT), m_pulseAudioSourceName)));
}

void UCameraThread::sl_takePicture(int _camera, QString _filename)
{
    if(m_cameras.count(_camera) == 1)
    {
        m_cameras[_camera]->TakePicture(_filename);
    }
}

void UCameraThread::sl_recordVideo(int _camera, QString _filename, QString _bitrate, QString _x264Preset)
{
    if(m_cameras.count(_camera) == 1)
    {
        m_cameras[_camera]->RecordVideo(_filename, _bitrate, _x264Preset);
    }
}

void UCameraThread::sl_stopRecord(int _camera)
{
    if(m_cameras.count(_camera) == 1)
    {
        m_cameras[_camera]->StopRecord();
    }
}

void UCameraThread::ReadPulseAudioSource()
{
    QFile f("/reetiPrograms/configuration/audio_pulse_source");
    if (!f.open(QFile::ReadOnly | QFile::Text))
    {
        qWarning() << "Cannot read " << f.fileName();
        qWarning() << "using default audio input";
        m_pulseAudioSourceName = "default";
    }
    else
    {
        QTextStream in(&f);
        m_pulseAudioSourceName = in.readLine();
        m_pulseAudioSourceName = m_pulseAudioSourceName.replace("\r\n", "");
        m_pulseAudioSourceName = m_pulseAudioSourceName.replace("\n", "");
    }
}

QString UCameraThread::GetCameraDevFromId(int _camera)
{
    if(_camera == 0)
    {
        return QString(CAMERA_PATH_LEFT);
    }
    else
    {
        return QString(CAMERA_PATH_RIGHT);
    }
}
