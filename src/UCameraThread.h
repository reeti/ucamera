#ifndef CAMERATHREAD_H
#define CAMERATHREAD_H

#define CAMERA_PATH_LEFT "/dev/video_left_lo"
#define CAMERA_PATH_RIGHT "/dev/video_right_lo"

#include <QObject>
#include <QUrl>
#include <QFile>
#include <QProcess>
#include <memory>
#include "CameraInterface.h"

class UCameraThread : public QObject
{
    Q_OBJECT
public:
    UCameraThread(QObject *_parent = 0);
    virtual ~UCameraThread();

public slots:
    void sl_Init();

    void sl_takePicture(int _camera, QString _filename);
    void sl_recordVideo(int _camera, QString _filename, QString _bitrate, QString _x264Preset);
    void sl_stopRecord(int _camera);

private:
    void ReadPulseAudioSource();
    QString GetCameraDevFromId(int _camera);

    std::map<int, CameraInterface*> m_cameras;
    QString m_pulseAudioSourceName;
};

#endif
